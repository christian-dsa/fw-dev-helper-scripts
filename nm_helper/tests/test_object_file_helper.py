"""
 Test object_file_helper.py
"""
# pylint: disable=missing-function-docstring

import filecmp
import os
from shutil import copyfile
from nm_helper.nm_helper import (generate_symbols_for_object_file,
                                 parse_symbols_file,
                                 pretty_symbols_file,
                                 get_sections_total_size)

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
SUPPORT_PATH = f"{DIR_PATH}/support"
RESULTS_PATH = f"{DIR_PATH}/results"


def test_generate_symbols_for_object_file():
    # Setup
    input_file = f"{SUPPORT_PATH}/libfortest.a"
    output_file = f"{RESULTS_PATH}/nm_output.txt"
    expected_output_file = f"{SUPPORT_PATH}/expected_libfortest_nm_output.txt"
    input_file_name = "libfortest.a"

    # Copy library next to executable to avoid having the whole file path in nm output file.
    copyfile(input_file, input_file_name)

    # Test
    generate_symbols_for_object_file(input_file_name, output_file)

    # Assert
    assert filecmp.cmp(output_file, expected_output_file)

    # Clean up
    os.remove(output_file)
    os.remove(input_file_name)


def test_pretty_nm_output():
    # Setup
    input_file = f"{SUPPORT_PATH}/libfortest.a"
    nm_output_file = f"{RESULTS_PATH}/nm_output.txt"
    pretty_output_file = f"{RESULTS_PATH}/nm_pretty_output.txt"
    expected_output_file = f"{SUPPORT_PATH}/expected_libfortest_nm_pretty_output.txt"
    input_file_name = "libfortest.a"

    # Copy library next to executable to avoid having the whole file path in nm output file.
    copyfile(input_file, input_file_name)

    # Generate symbols file
    generate_symbols_for_object_file(input_file_name, nm_output_file)

    # Test
    symbols = parse_symbols_file(nm_output_file)
    pretty_symbols_file(symbols, pretty_output_file)

    # Assert
    assert filecmp.cmp(pretty_output_file, expected_output_file)

    # Clean up
    os.remove(nm_output_file)
    os.remove(input_file_name)
    os.remove(pretty_output_file)


def test_get_sections_total_size():
    # Setup
    input_file = f"{SUPPORT_PATH}/libfortest.a"
    nm_output_file = f"{RESULTS_PATH}/nm_output.txt"
    expected_section_size = {'.data': 120, '.rodata': 88, '.text': 25604}

    # Generate symbols file
    generate_symbols_for_object_file(input_file, nm_output_file)

    # Test
    symbols = parse_symbols_file(nm_output_file)
    sections_size = get_sections_total_size(symbols)

    # Assert
    assert sections_size == expected_section_size

    # Clean up
    os.remove(nm_output_file)
